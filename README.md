# Ansible role for moving data on a specific mount point

## Introduction

This role moves data at some new location, presumably on another mountpoint/drive, and use a mount-bind to make it available at the original path. It is able to stop/start a list of affected services using these data to ensure consistency (but not uptime).

Beware the new path has to be non-existant, so moving data at the top of a mountpoint is not supported.

## Variables

  - **src**: original path of the data
  - **dest**: new path of the data
  - **services**: list of sysinit/systemd service names to be stopped before and restarted after operation

